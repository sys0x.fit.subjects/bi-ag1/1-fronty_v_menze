#include <iostream>
#include <map>
#include <vector>
#include <set>
#include <fstream>
#include <list>
#include <queue>

using namespace std;
void wrongInput(){
    cout<<"Wrong input!"<<endl;
}
class Node{
private:
    vector<Node *> neighbours;
    int depth;//in what position of a line is the person standing
    bool canBeAccessed;
    int numberOfStepsToStart=0;
public:
    int getNumberOfStepsToStart() const {
        return numberOfStepsToStart;
    }

    Node(int depth, bool canBeAccessed) : depth(depth), canBeAccessed(canBeAccessed) {}
    void addNeighbour(Node * node){
        neighbours.push_back(node);
    }
    bool isAccesible(){
        return canBeAccessed;
    }
     void getNeighbours(queue<Node *> & queue){
        visited();
        for(auto node:neighbours){
            if(node->canBeAccessed&&node->depth>numberOfStepsToStart){
                queue.push(node);
                node->numberOfStepsToStart=numberOfStepsToStart+1;
                node->visited();
            }

        }

    }
    void visited(){
        canBeAccessed= false;
    }

};
class Tree{
private:
    int time=0;
    int lineLength;
    int skipLength;
    vector<Node* >lineLeft ,lineRight;
    Node * finish;
public:

    Tree(int lineLength, int skipLength) : lineLength(lineLength), skipLength(skipLength) {
    lineLeft.reserve(lineLength);
    lineRight.reserve(lineLength);
    finish = new Node(lineLength+1,true);
    }
    int getLineLength(){
        return lineLength;
    }

    void nextStep(){
        time++;
    }
    int getTime() const {
        return time;
    }

    Node *getFinish() const {
        return finish;
    }
    Node *getStart() const {
        return lineLeft[0];
    }

    void createNode(int depth,bool canBeAccessed) {
        Node *newNode=new Node(depth, canBeAccessed);
        if (lineLeft.size() < (unsigned)lineLength) {
            lineLeft.push_back(newNode);
        }else{
            lineRight.push_back(newNode);
        }
    }

    void createStructureLeft(){

        for(int i=0;i<lineLength;i++){
            if(i>0&&lineLeft[i-1]->isAccesible()) {//-1
                lineLeft[i]->addNeighbour(lineLeft[i-1]);
            }

            if((i-skipLength)>0&&lineRight[i-skipLength]->isAccesible()){//-jumplength
                lineLeft[i]->addNeighbour(lineRight[i-skipLength]);
            }

            if(i<(lineLength-1)){//+1
                if(lineLeft[i+1]->isAccesible()){
                    lineLeft[i]->addNeighbour(lineLeft[i+1]);
                }
            }else{
                lineLeft[i]->addNeighbour(finish);
            }

            if((i+skipLength)<lineLength){//+jumplength
                if(lineRight[i+skipLength]->isAccesible()) {
                    lineLeft[i]->addNeighbour(lineRight[i + skipLength]);
                }
            } else{
                lineLeft[i]->addNeighbour(finish);
            }
        }
    }
    void createStructureRight(){

        for(int i=0;i<lineLength;i++){
            if(i>0&&lineRight[i-1]->isAccesible()) {//-1
                lineRight[i]->addNeighbour(lineRight[i-1]);
            }

            if((i-skipLength)>0&&lineLeft[i-skipLength]->isAccesible()){//-jumplength
                lineRight[i]->addNeighbour(lineLeft[i-skipLength]);
            }

            if(i<(lineLength-1)){//+1
                if(lineRight[i+1]->isAccesible()){
                    lineRight[i]->addNeighbour(lineRight[i+1]);
                }
            }else{
                lineRight[i]->addNeighbour(finish);
            }

            if((i+skipLength)<lineLength){//+jumplength
                if(lineLeft[i+skipLength]->isAccesible()) {
                    lineRight[i]->addNeighbour(lineLeft[i + skipLength]);
                }
            } else{
                lineRight[i]->addNeighbour(finish);
            }
        }
    }

    ~Tree(){
        //todo
    }


};
bool getLine(Tree *tree){

    char per;//dot or x

    for (int i = 0; i < tree->getLineLength(); i++) {
        cin >> per;
        switch (per) {
            case '.': {
                tree->createNode(i,true);
                break;
            }
            case 'x': {
                tree->createNode(i,false);
                break;
            }
            default: {
                return false;
            }
        }
    }
    return true;
}
Tree getInput(){
    Tree *tree;
    int lineLength;
    int skipLength;
    cin>>lineLength>>skipLength;
    if(lineLength<skipLength||skipLength<=1){
        //wrong input
    }
    ////creating tree!!!!!!!
    tree =new Tree(lineLength,skipLength);
    ////creating tree!!!!!!!

    //first is leftLine
    getLine(tree);
    //then is rightLine
    getLine(tree);

    tree->createStructureLeft();
    tree->createStructureRight();

    return *tree;
}

int BFS(Tree & tree){
    Node * finish=tree.getFinish();
    Node * start= tree.getStart();
    queue<Node *> qu;
    qu.push(start);
    start->visited();
    Node * temp;
    while(!qu.empty()){

        temp=qu.front();
        qu.pop();
        temp->getNeighbours(qu);
        if(temp==finish){
            return temp->getNumberOfStepsToStart();
        }

    }
    return -1;


}


int main() {

    Tree tree=getInput();
    cout<<BFS(tree)<<endl;
    return 0;
}


